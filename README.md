# Head pose estimation

Use CNN and OpenCV to estimate head poses.

CNN stands for Convolutional Neural Network. It is used to detect facial landmarks in image.

Considering human faces are roughly same in facial landmark distribution, OpenCV provides mutual PnP algorithms which could be adopted for pose estimation.

